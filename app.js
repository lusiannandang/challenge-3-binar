const express = require("express");
const app = express();
const port = process.env.PORT || 8000;

app.set("view engine", "ejs");
app.use(express.static("public"));

// app.use("/", Routes);

app.get("/", (req, res) => {
  res.render("index", { title: "Login" });
});

app.get("/dashboard", (req, res) => {
  res.render("dashboard", { title: "Dashboard" });
});

app.get("/carDashboard", (req, res) => {
  res.render("carDashboard", { title: "Car" });
});

app.get("/carSuccess", (req, res) => {
  res.render("carSuccess", { title: "Car" });
});

app.get("/addCar", (req, res) => {
  res.render("addCar", { title: "addCar" });
});

app.listen(port, () => {
  console.log("Server running on port 8000");
});
