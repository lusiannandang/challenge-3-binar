const { Router } = require("express");
const router = Router();
const satpam = require("../middlewares/AuthMiddlewares");

router.get("/", satpam, (req, res) => {
  res.render("login");
});

module.exports = router;
